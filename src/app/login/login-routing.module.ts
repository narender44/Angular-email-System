import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login.component";
import { MysignUpComponent } from "../mysign-up/mysign-up.component";
import { HeaderComponent } from "../header/header.component";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { EmailTermsComponent } from "../email-terms/email-terms.component";
const routes: Routes = [
  {
    path: "",
    component: EmailTermsComponent
  },

  {
    path: "login",
    component: LoginComponent
  },

  {
    path: "signup",
    component: MysignUpComponent
  },
  {
    path: "forgotPass",
    component: ForgotPasswordComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}
