import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./login.component";
import { MysignUpComponent } from "../mysign-up/mysign-up.component";
import { LoginRoutingModule } from "./login-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
@NgModule({
  declarations: [LoginComponent, MysignUpComponent],
  imports: [CommonModule, LoginRoutingModule, ReactiveFormsModule, FormsModule]
})
export class LoginModule {}
