import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, FormBuilder, NgForm } from "@angular/forms";
import * as EmailValidator from "email-validator";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
import { InboxdataService } from "../service/inboxdata.service";

import { ToastrService } from "ngx-toastr";
import { HttpService } from "../service/http.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
/**
 * post method basically used for Login validation
 * It also check the localstorage Data so easily identify the user
 * sweeat alert basically used for pop up
 * Authentication:EmailValidator Third party package and password alphabetic &num combination
 */
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  email: string;
  password: string = "";
  myLoginUserArray = [];
  constructor(
    private _frmBuilder: FormBuilder,
    public router: Router,
    private toaster: ToastrService,
    private _http: HttpService,
    private _Inbox: InboxdataService
  ) {
    // this._apicall.getName().subscribe(data => console.log("+++++", data));
  }
  post(myLoginData: any) {
    var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    this.myLoginUserArray = JSON.parse(localStorage.getItem("user"));

    if (
      EmailValidator.validate(this.loginForm.controls.email.value) &&
      this.loginForm.controls.password.value.length > 7 &&
      regularExpression.test(this.loginForm.controls.password.value)
    ) {
      this._http.login(
        this.loginForm.controls.email.value,
        this.loginForm.controls.password.value
      );
      this.myLoginUserArray.map(ed => {
        if (
          ed.Email === this.loginForm.controls.email.value &&
          ed.password === this.loginForm.controls.password.value
        ) {
          this.router.navigate(["/afterSignin/inbox"]);
          this._Inbox.myUser(ed.Email);
        } else {
          console.log("Not hello");
        }
      });
    } else {
      this.toaster.info(
        "It looks like you fill wrong Information",
        "your email-Id or Password are wrong",
        {
          progressBar: true,
          positionClass: "toast-bottom-center"
        }
      );
    }
  }
  ngOnInit(): void {
    this.loginForm = this._frmBuilder.group({
      email: new FormControl(),
      password: new FormControl()
    });
  }
}
