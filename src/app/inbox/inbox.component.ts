import { Component, OnInit } from "@angular/core";
import { EmailDeatilsService } from "../service/email-deatils.service";
import { InboxdataService } from "../service/inboxdata.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-inbox",
  templateUrl: "./inbox.component.html",
  styleUrls: ["./inbox.component.css"]
})
export class InboxComponent implements OnInit {
  my_email = [];
  myUser: any;
  p: number = 1;
  constructor(
    private _EmailService: EmailDeatilsService,
    private _InboxData: InboxdataService,
    public router: Router
  ) {
    console.log("Gajjar Darshit Inbox Page");
  }
  callMe(item: any) {
    this._EmailService.checkReadStatus(item);
    this.my_email.map(de => {
      if (de.id === item) {
        this._EmailService.myEmailData(de);
      }
    });
  }
  deleteEmail(deletNumber: any) {
    this._InboxData.remove(deletNumber);
    this.router.navigate(["/afterSignin/inbox"]);
  }
  ngOnInit(): void {
    this.myUser = this._InboxData.myUserReturn();
    if (this.myUser == undefined || this.myUser == null) {
      this.router.navigate(["/layout/login"]);
    }

    this._InboxData.myEmaileData().map(df => {
      if (this.myUser == df.to && df.Inbox == false) {
        this.my_email.push(df);
      }
    });
    // console.log("-->", this.my_email);
  }
}
