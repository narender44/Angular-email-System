import { NgModule } from "@angular/core";
import { InboxComponent } from "./inbox.component";
import { FormsModule } from "@angular/forms";

import { InboxRoutingModule } from "./Inbox-routing.module";
import { EmailDeatilsService } from "../service/email-deatils.service";
import { CommonModule } from "@angular/common";
import { TrashComponent } from "../trash/trash.component";
import { OutboxComponent } from "../outbox/outbox.component";
import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
  declarations: [InboxComponent, TrashComponent, OutboxComponent],
  imports: [InboxRoutingModule, FormsModule, CommonModule, NgxPaginationModule]
  //jyare je routing file import karavo tene import ma declare karavi important che
})
export class InboxModule {}
