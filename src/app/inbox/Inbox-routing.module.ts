import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { InboxComponent } from "../inbox/inbox.component";
import { OutboxComponent } from "../outbox/outbox.component";
import { TrashComponent } from "../trash/trash.component";
import { EmailsDetailsComponent } from "../emails-details/emails-details.component";

const routes: Routes = [
  {
    path: "inbox",
    component: InboxComponent,
    pathMatch: "full"
  },
  {
    path: "outbox",
    component: OutboxComponent
  },
  {
    path: "trash",
    component: TrashComponent
  },
  {
    path: "inbox/emaildetails",
    component: EmailsDetailsComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InboxRoutingModule {}
