import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysignUpComponent } from './mysign-up.component';

describe('MysignUpComponent', () => {
  let component: MysignUpComponent;
  let fixture: ComponentFixture<MysignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
