import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, NgForm, FormBuilder } from "@angular/forms";
import * as EmailValidator from "email-validator";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";
import { HttpService } from "../service/http.service";
import { InboxdataService } from "../service/inboxdata.service";

@Component({
  selector: "app-mysign-up",
  templateUrl: "./mysign-up.component.html",
  styleUrls: ["./mysign-up.component.css"]
})
export class MysignUpComponent implements OnInit {
  signupForm: FormGroup;
  Email: string;
  password: string;
  retype: string;
  username: string;
  IdentityUser: string;
  myUser = {
    username: "",
    Email: "",
    password: "",
    retype: ""
  };
  myUserArray = [];
  tempUserArray = [];
  constructor(
    public frmBuilder: FormBuilder,
    public router: Router,
    private _myUser: InboxdataService,
    private toaster: ToastrService,
    private _apicall: HttpService
  ) {}
  ngOnInit(): void {
    this.signupForm = this.frmBuilder.group({
      username: new FormControl(),
      Email: new FormControl(),
      password: new FormControl(),
      retype: new FormControl()
    });
  }
  showToast() {}

  /*
  * mysignup.component.html form call and check password 
  * alphabetic with numeric 
    and data store in local-storage
  */
  myData(signupForm: any) {
    var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    /**
     * this.tempUserArray null if data is not
     */
    this.tempUserArray = JSON.parse(localStorage.getItem("user"));

    if (this.tempUserArray != null) {
      this.tempUserArray.map(data => {
        this.myUserArray.push(data);
      });
    }

    if (
      EmailValidator.validate(signupForm.controls.Email.value) &&
      signupForm.controls.password.value.length > 7 &&
      regularExpression.test(signupForm.controls.password.value) &&
      signupForm.controls.password.value === signupForm.controls.retype.value
    ) {
      this.myUser.username = signupForm.controls.Email.value;
      this.myUser.Email = signupForm.controls.Email.value;
      this.myUser.password = signupForm.controls.password.value;
      this.myUser.retype = signupForm.controls.retype.value;
      this._apicall.signup(
        this.myUser.username,
        this.myUser.Email,
        this.myUser.password,
        this.myUser.retype
      );
      /**
       * check user account have or not
       */
      if (this.myUserArray.length > 0) {
        this.myUserArray.map(myUser => {
          if (
            myUser.Email === this.myUser.Email &&
            myUser.password != this.myUser.password
          ) {
            this.router.navigate(["/layout/forgotPass"]);
          } else if (
            myUser.Email === this.myUser.Email &&
            myUser.password === this.myUser.password
          ) {
            this.toaster.error(
              "User you aleready login",
              "show go your login page and enter your credential",
              {
                positionClass: "toast-bottom-center",
                easing: "easy-out"
              }
            );
          } else {
            this.myUserArray.push(this.myUser);
            // localStorage.setItem("user", JSON.stringify(this.myUserArray));

            this._myUser.myUser(signupForm.controls.Email.value); // send the User value to the Identify user service
            // this.router.navigate(["/afterSignin/inbox"]); //after navigate storing Data in localStorage
          }
        });
      } else {
        this.myUserArray.push(this.myUser);
        // localStorage.setItem("user", JSON.stringify(this.myUserArray));

        this._myUser.myUser(signupForm.controls.Email.value); // send the User value to the Identify user service
        // this.router.navigate(["/afterSignin/inbox"]); //after navigate storing Data in localStorage
      }

      /**
       * end
       */
    } else {
      this.toaster.error(
        "something went wrong",
        `${this.signupForm.controls.Email.value} or ${this.signupForm.controls.password.value} are wrong`,
        {
          positionClass: "toast-bottom-center",
          easing: "easy-out"
        }
      );
    }
  }
}
