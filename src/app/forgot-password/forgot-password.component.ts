import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.css"]
})
export class ForgotPasswordComponent implements OnInit {
  constructor(private toaster: ToastrService) {}

  ngOnInit(): void {}
  forgot() {
    this.toaster.success("we send the email", "", {
      positionClass: "toast-bottom-center",
      easing: "easy-out"
    });
  }
}
