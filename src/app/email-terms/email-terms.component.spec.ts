import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTermsComponent } from './email-terms.component';

describe('EmailTermsComponent', () => {
  let component: EmailTermsComponent;
  let fixture: ComponentFixture<EmailTermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
