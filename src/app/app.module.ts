/**
 * angular 9.0.0 version
 */

import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NavbarComponent } from "./navbar/navbar.component";
import { InboxModule } from "./inbox/inbox.module";
import { AfterSigninComponent } from "./after-signin/after-signin.component";
import { EmailDeatilsService } from "./service/email-deatils.service";
import { InboxdataService } from "./service/inboxdata.service";
import { CommonModule } from "@angular/common";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LoginModule } from "./login/login.module";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { EmailTermsComponent } from "./email-terms/email-terms.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { ToastrModule, ToastContainerModule } from "ngx-toastr";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    AfterSigninComponent,
    ForgotPasswordComponent,
    EmailTermsComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    InboxModule,
    NgbModule,
    LoginModule,
    ToastrModule.forRoot(),
    ToastContainerModule
  ],
  providers: [EmailDeatilsService, InboxdataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
