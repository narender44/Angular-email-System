import { Component, Inject } from "@angular/core";
import { ActivatedRoute, Route } from "@angular/router";
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "AngularEmailSystem";
  constructor(public route: ActivatedRoute) {}
}
