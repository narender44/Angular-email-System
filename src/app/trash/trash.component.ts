import { Component, OnInit } from "@angular/core";
import { InboxdataService } from "../service/inboxdata.service";
import { CommonModule } from "@angular/common";
@Component({
  selector: "app-trash",
  templateUrl: "./trash.component.html",
  styleUrls: ["./trash.component.css"]
})

/**
 *  InboxFolder/Inboxdata.service.ts has TrashMessage method and return TrashMessage Array
 *  df is object that one by one push
 */
export class TrashComponent implements OnInit {
  myUser: any;
  p: number = 1;
  trashMessage = [];
  constructor(private _InboxService: InboxdataService) {}
  ngOnInit(): void {
    this.myUser = this._InboxService.myUserReturn();

    this._InboxService.myEmaileData().map(df => {
      if (this.myUser == df.to && df.Inbox == true) {
        this.trashMessage.push(df);
      }
    });
  }
}
