import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormControl, FormBuilder, NgForm } from "@angular/forms";
import { InboxdataService } from "../service/inboxdata.service";
import * as EmailValidator from "email-validator";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-after-signin",
  templateUrl: "./after-signin.component.html",
  styleUrls: ["./after-signin.component.css"]
})
export class AfterSigninComponent implements OnInit {
  closeResult: string;
  myEmailData: FormGroup;
  To: string;
  Subject: string;
  Description: string;
  myUserEmail: string;
  urcl: any = "";
  myUserInfo: any = {
    myEmail: "",
    id: "",
    to: "",
    Exercise: "",
    Time: "",
    read: false,
    outbox: false,
    Inbox: false
  };
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private _modalService: NgbModal,
    private _frmBuilder: FormBuilder,
    private _inboxService: InboxdataService,
    private toaster: ToastrService
  ) {}
  ngOnInit(): void {
    if (this._inboxService.myUserReturn() != null) {
      this.myUserEmail = this._inboxService.myUserReturn().split("@")[0];
      this.urcl = localStorage.getItem(this.myUserEmail);
    }
    console.log("Hello Darshit Have nice Day ");

    this.myEmailData = this._frmBuilder.group({
      To: new FormControl(),
      Subject: new FormControl(),
      Description: new FormControl()
    });
  }
  open(content) {
    this._modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  storeWallpaper() {
    localStorage.setItem(this.myUserEmail, this.urcl);
    this._modalService.dismissAll();
  }
  theme(content) {
    this._modalService
      .open(content, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg"
      })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  userWallpaper(event: any) {
    this.urcl = event.target.src;
  }
  logout() {
    localStorage.removeItem("LoggedIn");
  }
  mySendEmailData(myEmailData: any) {
    var dt = new Date();
    var utcDate = dt.toUTCString();
    let uid = "";
    let id = "wtftsvsg17621018@$%^(@)!@!(";
    for (let i = 0; i < 6; i++) {
      uid = uid + id.charAt(Math.floor(Math.random() * id.length));
    }
    this.myUserInfo.myEmail = this._inboxService.myUserReturn();
    this.myUserInfo.id = uid;
    this.myUserInfo.to = myEmailData.value.To;
    this.myUserInfo.Time = utcDate;
    this.myUserInfo.Exercise = myEmailData.value.Subject;
    this.myUserInfo.Description = myEmailData.value.Description;
    //data is store in File
    console.log("this.myUserInfo:: ", this.myUserInfo);
    const test = Object.assign({}, this.myUserInfo); //Exactly

    if (EmailValidator.validate(this.myUserInfo.to)) {
      this._inboxService.pushMyEmail(test);
      this._modalService.dismissAll();
    } else {
      this.toaster.error("It looks like your Email is wrong", "", {
        progressBar: true,
        positionClass: "toast-bottom-center"
      });
    }
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
