import { Component, OnInit } from "@angular/core";
import { EmailDeatilsService } from "../service/email-deatils.service";
@Component({
  selector: "app-emails-details",
  templateUrl: "./emails-details.component.html",
  styleUrls: ["./emails-details.component.css"]
})
export class EmailsDetailsComponent implements OnInit {
  myDataValue: any;
  constructor(public _myEmaildata: EmailDeatilsService) {}

  ngOnInit(): void {
    this.myDataValue = this._myEmaildata.callMe();
    console.log("--->", this.myDataValue);
  }
}
