import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailsDetailsComponent } from './emails-details.component';

describe('EmailsDetailsComponent', () => {
  let component: EmailsDetailsComponent;
  let fixture: ComponentFixture<EmailsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
