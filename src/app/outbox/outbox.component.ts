import { Component, OnInit } from "@angular/core";
import { InboxdataService } from "../service/inboxdata.service";
import { EmailDeatilsService } from "../service/email-deatils.service";

@Component({
  selector: "app-outbox",
  templateUrl: "./outbox.component.html",
  styleUrls: ["./outbox.component.css"]
})
export class OutboxComponent implements OnInit {
  my_email = [];
  myUser: any;
  p: number = 1;
  constructor(
    private _OutboxData: InboxdataService,
    private _EmailService: EmailDeatilsService
  ) {
    console.log("outbox is start");
  }
  callMe(item: any) {
    this.my_email.map(de => {
      if (de.id === item) {
        this._EmailService.myEmailData(de);
      }
    });
  }
  ngOnInit(): void {
    this.myUser = this._OutboxData.myUserReturn();
    console.log("my User", this.myUser);

    this._OutboxData.myEmaileData().map(df => {
      console.log("df Email", df.myEmail);

      if (df.myEmail == this.myUser && df.outbox == false) {
        this.my_email.push(df);
      }
    });
  }
  outboxEmail(id: any) {
    this._OutboxData.outboxremove(id);
  }
}
