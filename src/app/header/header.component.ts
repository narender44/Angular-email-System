import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, NgForm } from "@angular/forms";
import * as EmailValidator from "email-validator";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  LoginForm: FormGroup;
  email: string = "";
  password: string = "";
  loginNow: boolean = false;
  identifyUser: string;
  constructor(private router: Router, public route: ActivatedRoute) {}

  ngOnInit(): void {
    this.identifyUser = localStorage.getItem("LoggedIn");
    if (this.identifyUser == null && this.identifyUser == undefined) {
      this.router.navigate(["/layout"]);
    } else {
      this.router.navigate(["/afterSignin/inbox"]);
    }
  }
  ngOnDestroy(): void {
    console.log("My route is call another");
  }
}
