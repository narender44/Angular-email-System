import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserData } from "../observable/observable";
@Injectable({
  providedIn: "root"
})
export class HttpService {
  private _url: string = "http://localhost:3000/v1/user/register";
  constructor(private http: HttpClient) {}

  signup(username: any, email: any, password: any, retype: any): void {
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    console.log("Gajju Rocks");

    this.http
      .post(
        "http://localhost:3000/v1/user/register",
        {
          username: username,
          email: email,
          password: password
        },
        options
      )
      .subscribe(data => {
        console.log("}--->", data);
      });
  }
  login(email: any, password: any) {
    this.http
      .post<any>("http://localhost:3000/api/login", {
        EmailId: email,
        Password: password
      })
      .subscribe(data => {
        console.log("}--->}}}", data);
      });
  }
}
