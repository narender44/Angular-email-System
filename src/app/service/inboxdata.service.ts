export class InboxdataService {
  my_email: any = [];
  temp_email: any = [];
  show_email: any = [];
  deleteMessage: any;
  TrashMessage = [];
  tempTrashMessage: any = [];
  myAppUserEmail: string;

  myEmaildata() {
    return this.my_email;
  }
  remove(number: any) {
    console.log("", number);

    this.tempTrashMessage = localStorage.getItem("Inbox");
    this.my_email.map(emailData => {
      if (emailData.id == number) {
        emailData.Inbox = true;
      }
    });
    localStorage.setItem("Inbox", JSON.stringify(this.my_email));

    // this.deleteMessage = this.my_email.splice(
    //   this.my_email.findIndex(de => de.id == number),
    //   1
    // );

    // localStorage.setItem("Inbox", JSON.stringify(this.my_email));
    // if (this.temp_email != null) {
    //   this.TrashMessage.push(this.deleteMessage[0]);

    //   localStorage.setItem("Trash", JSON.stringify(this.TrashMessage));
    // } else {
    //   this.TrashMessage.push(this.deleteMessage);

    //   localStorage.setItem("Trash", JSON.stringify(this.TrashMessage));
    // }
  }
  outboxremove(id: any) {
    console.log("outbox1", this.my_email);

    this.my_email.map(emailData => {
      if (emailData.id == id) {
        emailData.outbox = true;
      }
    });

    localStorage.setItem("Inbox", JSON.stringify(this.my_email));
  }
  TrashMyMessage() {
    this.tempTrashMessage = localStorage.getItem("Trash");

    if (this.tempTrashMessage != null) {
      this.TrashMessage = JSON.parse(localStorage.getItem("Trash"));
    }
    return this.TrashMessage;
  }
  myEmaileData() {
    this.temp_email = localStorage.getItem("Inbox");

    if (this.temp_email != null) {
      this.my_email = JSON.parse(localStorage.getItem("Inbox"));
    }
    return this.my_email;
  }
  pushMyEmail(message: any) {
    this.temp_email = localStorage.getItem("Inbox");
    console.log("-->", this.my_email);
    console.log(message);

    if (this.temp_email != null) {
      this.my_email.push(message);
      console.log("---", this.my_email);

      localStorage.setItem("Inbox", JSON.stringify(this.my_email));
    } else {
      this.my_email.push(message);
      console.log("---:::", this.my_email);

      localStorage.setItem("Inbox", JSON.stringify(this.my_email));
    }
  }
  myUser(user: any) {
    if (this.myAppUserEmail == null) {
      localStorage.setItem("LoggedIn", user);
    } else {
      localStorage.setItem("LoggedIn", this.myAppUserEmail);
    }
  }
  myUserReturn(): string {
    if (this.myAppUserEmail == null) {
      this.myAppUserEmail = localStorage.getItem("LoggedIn");
    }

    return this.myAppUserEmail;
  }
  constructor() {
    this.myAppUserEmail = localStorage.getItem("LoggedIn");
  }
}
