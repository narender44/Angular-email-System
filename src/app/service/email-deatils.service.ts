import { Injectable, OnInit } from "@angular/core";

//Injectable create the dependency Injection so that create the new Instance in services
//so if you want to move central Instance so that time Injectable is not used for that
// @Injectable({
//   providedIn: "root"
// })
export class EmailDeatilsService {
  myDataValue: any = "";
  myEmails: any = [];

  myEmailData(emailData: any) {
    this.myDataValue = emailData;
    console.log(this.myDataValue);
  }
  callMe() {
    return this.myDataValue;
  }
  checkReadStatus(id: any) {
    this.myEmails = JSON.parse(localStorage.getItem("Inbox"));
    this.myEmails.map(ed => {
      if (ed.id === id) {
        ed.read = true;
      }
    });
    localStorage.setItem("Inbox", JSON.stringify(this.myEmails));
  }
}
