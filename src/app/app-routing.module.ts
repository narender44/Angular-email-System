import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// import { MysignUpComponent } from "./mysign-up/mysign-up.component";
import { AfterSigninComponent } from "./after-signin/after-signin.component";
import { InboxComponent } from "./inbox/inbox.component";
import { TrashComponent } from "./trash/trash.component";
import { HeaderComponent } from "./header/header.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "layout",
    pathMatch: "full"
  },
  {
    path: "layout",
    component: HeaderComponent,
    children: [
      {
        path: "",

        loadChildren: () =>
          import("./login/login.module").then(m => m.LoginModule)
      }
    ]
  },

  {
    path: "afterSignin",
    component: AfterSigninComponent,
    children: [
      {
        path: "",

        loadChildren: () =>
          import("./inbox/inbox.module").then(m => m.InboxModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
